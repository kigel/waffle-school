package ddosscanner

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/kigel/waffle/mongomanager"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DdosScanner struct {
	MongoManager              *mongomanager.MongoManager
	min_count                 int
	mul_value                 int
	min_reqs_per_client       int
	min_percentage_per_client int
	time_period               int
}

func (dc *DdosScanner) initDc() {
	dc.min_count = dc.MongoManager.Settings.DdosScanner.MinCount
	dc.mul_value = dc.MongoManager.Settings.DdosScanner.MulValue
	dc.min_reqs_per_client = dc.MongoManager.Settings.DdosScanner.MinReqsPerClient
	dc.min_percentage_per_client = dc.MongoManager.Settings.DdosScanner.MinPercentagePerClient
	dc.time_period = dc.MongoManager.Settings.DdosScanner.TimePeriod
}

// Start scanning for ddos
func (dc *DdosScanner) ScanDdos() {
	dc.initDc()
	// will scan for http flood every minute
	for {
		fmt.Println("Scanning For DDOS")
		// get all requests from the before last minute
		reqsTwoAgo, err := dc.MongoManager.Get("logs", "requests", bson.M{
			"createdAt": bson.M{
				"$gt": time.Now().Add(-2 * time.Duration(dc.time_period) * time.Second),
				"$lt": time.Now().Add(-1 * time.Duration(dc.time_period) * time.Second),
			},
		})
		if err != nil {
			continue
		}

		// get the max count of reqs allowed
		maxCountReqs := max(len(reqsTwoAgo)*dc.mul_value, dc.min_count)

		// get all requests from the last minute
		reqsOneAgo, err := dc.MongoManager.Get("logs", "requests", bson.M{
			"createdAt": bson.M{
				"$gt": time.Now().Add(-1 * time.Duration(dc.time_period) * time.Second),
			},
		})
		if err != nil {
			continue
		}

		// if reqs from minute ago are more than the maximum allowed
		if len(reqsOneAgo) > maxCountReqs {
			fmt.Println("Scanning For The Attackers...")
			dc.blacklistSusClients(reqsOneAgo)
		}

		time.Sleep(time.Second * time.Duration(dc.time_period))
	}
}

// Find suspicious clients
func (dc *DdosScanner) findSusClients(susRequests []primitive.M) []string {
	// Count word occurrences
	ipCount := make(map[string]int)
	for _, req := range susRequests {
		if count, ok := ipCount[req["ip"].(string)]; ok {
			ipCount[req["ip"].(string)] = count + 1
		} else {
			ipCount[req["ip"].(string)] = 1
		}
	}

	threshold := max(dc.min_reqs_per_client, int(float64(len(susRequests))*math.Floor(float64(dc.min_percentage_per_client))))

	var susClientAddr []string
	for key, value := range ipCount {
		if value > threshold {
			susClientAddr = append(susClientAddr, key)
		}
	}

	fmt.Println(susClientAddr)

	return susClientAddr
}

// Do action on the suspicious clients
func (dc *DdosScanner) blacklistSusClients(susRequests []primitive.M) {
	expiration := time.Now().AddDate(10, 0, 0)

	susClients := dc.findSusClients(susRequests)
	for i := 0; i < len(susClients); i++ {
		clientBson := bson.M{
			"ip":       susClients[i],
			"expireAt": expiration,
		}

		fmt.Println("Banning Potential Attackers")
		dc.MongoManager.Insert("data", "blacklist", clientBson)
	}
}
