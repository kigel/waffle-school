package mongomanager

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/kigel/waffle/settings"
)

type MongoManager struct {
	Settings *settings.Settings
	Client   *mongo.Client
}

// Connects to the mongodb server, creates a client to the struct for communication
func (mngman *MongoManager) Connect() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// Create the client
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://"+mngman.Settings.Database.Db_Adrress))
	if err != nil {
		return err
	}

	// Check if the client is ok
	err = client.Ping(ctx, nil)
	if err != nil {
		client.Disconnect(ctx)
		return err
	}

	// put the client into the struct
	mngman.Client = client

	// Make sure that all of the needed indexes are there
	err = mngman.checkIndexes()
	if err != nil {
		return err
	}

	return nil
}

func (mngman *MongoManager) Disconnect() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	err := mngman.Client.Disconnect(ctx)
	if err != nil {
		return err
	}

	return nil
}

// Gets a document from the db based on an interface filter
func (mngman *MongoManager) Get(db string, coll string, filter interface{}) ([]bson.M, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	cur, err := mngman.Client.Database(db).Collection(coll).Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	// check if documents were found
	try := cur.TryNext(ctx)
	if !try {
		return []bson.M{}, nil
	}

	var dataSlice []bson.M
	var tempData bson.M

	// setup the returned slice
	for {
		err = cur.Decode(&tempData)
		if err != nil {
			return nil, err
		}
		dataSlice = append(dataSlice, tempData)

		if !cur.Next(ctx) {
			break
		}
	}

	return dataSlice, nil
}

// Inserts a document into the db
func (mngman *MongoManager) Insert(db string, coll string, data interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// Insert the document
	_, err := mngman.Client.Database(db).Collection(coll).InsertOne(ctx, data)
	if err != nil {
		return err
	}

	return nil
}

// Updates a document in the db
func (mngman *MongoManager) Update(db string, coll string, filter, newData interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	collumn := mngman.Client.Database(db).Collection(coll)

	// update the document
	_, err := collumn.UpdateOne(ctx, filter, newData)
	if err != nil {
		return err
	}

	return nil
}

func (mngman *MongoManager) InsertRequest(req *http.Request, sus bool) error {
	bodyContent, err := io.ReadAll(req.Body)
	if err != nil {
		bodyContent = []byte{0}
	}

	// restore the body in the request
	req.Body = io.NopCloser(bytes.NewReader(bodyContent))

	reqBson := bson.M{
		"createdAt": time.Now(),
		"ip":        ParseAddrToIP(req.RemoteAddr),
		"method":    req.Method,
		"path":      req.URL.String(),
		"size":      req.ContentLength,
		"headers":   req.Header,
		"data":      string(bodyContent),
		"sus":       sus,
	}

	err = mngman.Insert("logs", "requests", reqBson)
	return err
}

func (mngman *MongoManager) InsertResponse(res *http.Response) error {
	bodyContent, err := io.ReadAll(res.Body)
	if err != nil {
		bodyContent = []byte{0}
	}

	// restore the body in the request
	res.Body = io.NopCloser(bytes.NewReader(bodyContent))

	resBson := bson.M{
		"createdAt":   time.Now(),
		"ip":          ParseAddrToIP(res.Request.RemoteAddr),
		"status_code": res.StatusCode,
		"size":        res.ContentLength,
		"headers":     res.Header,
		"data":        string(bodyContent),
	}

	err = mngman.Insert("logs", "responses", resBson)
	return err
}

func ParseAddrToIP(addr string) string {
	// get the ip
	addrParts := strings.Split(addr, ":")
	port := addrParts[len(addrParts)-1]

	// remove the port from the address
	ip := strings.Replace(addr, ":"+port, "", -1)
	// remove brackets if ipv6
	ip = strings.Replace(ip, "[", "", -1)
	ip = strings.Replace(ip, "]", "", -1)

	return ip
}
