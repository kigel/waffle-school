package mongomanager

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Checks that the relevent indexes are in the db
func (mngman *MongoManager) checkIndexes() error {

  // the indexes for the data db
	var DataIndexes = map[string][]mongo.IndexModel{
		"session_buckets": {
			{
				Keys:    bson.D{{Key: "createdAt", Value: 1}},
				Options: options.Index().SetName("createdAt").SetExpireAfterSeconds(mngman.Settings.Database.Bucket_TTL),
			},
		},
		"blacklist": {
			{
				Keys:    bson.D{{Key: "expireAt", Value: 1}},
				Options: options.Index().SetName("expireAt").SetExpireAfterSeconds(0),
			},
		},
	}

  // the indexes for the log db
	var LogIndexes = map[string][]mongo.IndexModel{
		"requests": {
			{
				Keys:    bson.D{{Key: "createdAt", Value: 1}},
				Options: options.Index().SetName("createdAt").SetExpireAfterSeconds(mngman.Settings.Database.Log_TTL),
			},
		},
		"responses": {
			{
				Keys:    bson.D{{Key: "createdAt", Value: 1}},
				Options: options.Index().SetName("createdAt").SetExpireAfterSeconds(mngman.Settings.Database.Log_TTL),
			},
		},
	}

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	client := mngman.Client

  // Interate over all of the indexes
	for collectionName, indexes := range DataIndexes {
		collection := client.Database("data").Collection(collectionName)

		for _, index := range indexes {
      // Remove the index if it already exists
			_, _ = collection.Indexes().DropOne(ctx, *index.Options.Name)
      // Create the index into the db
			_, err := collection.Indexes().CreateOne(ctx, index)
			if err != nil {
				return err
			}
		}
	}

  // same as above
	for collectionName, indexes := range LogIndexes {
		collection := client.Database("logs").Collection(collectionName)

		for _, index := range indexes {
			_, _ = collection.Indexes().DropOne(ctx, *index.Options.Name)
			_, err := collection.Indexes().CreateOne(ctx, index)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
