# XSS:

### Malicious:
<iframe onLoad iframe onLoad="javascript:javascript:alert(1)"></iframe onLoad>
<meta charset="x-imap4-modified-utf7">&ADz&AGn&AG0&AEf&ACA&AHM&AHI&AGO&AD0&AGn&ACA&AG8Abg&AGUAcgByAG8AcgA9AGEAbABlAHIAdAAoADEAKQ&ACAAPABi
<IMG SRC="javascript:javascript:alert(1);">
<img onerror="javascript:alert(1)"></img>

### Ok:
<h1>This is fine!</h1>

# SQLi:

### Malicious:
' or 1=1#
" OR "" = "
1' ORDER BY 3--+

### Ok:
1
2
3
4
