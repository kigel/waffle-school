package proxy

import (
	"context"
	"io"
	"log"
	"net/http"

	"gitlab.com/kigel/waffle/errorer"
	"gitlab.com/kigel/waffle/mongomanager"
	"gitlab.com/kigel/waffle/settings"
)

type Proxy struct {
	Settings     *settings.Settings
	MongoManager *mongomanager.MongoManager
	ErrCh        chan errorer.Errorer
	CancelCtx    context.Context
	Server       *http.Server
}

func (prx *Proxy) Start() {
	// set the handlers
	mux := http.NewServeMux()
	mux.HandleFunc("/", prx.rootHandler())

	// check if the app server is on
	log.Print("Pinging web app server...")
	err := prx.pingApp()
	if err != nil {
		prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
		return
	}
	log.Print("Web app server ping successful!")

	srv := &http.Server{
		Addr:    prx.Settings.Proxy.Out_Adrress,
		Handler: mux,
	}

	prx.Server = srv

	// use ssl if there are ssl key and crt
	if prx.Settings.Proxy.Ssl_Crt != "" && prx.Settings.Proxy.Ssl_Key != "" {
		log.Print("Listening HTTPS...")
		err = http.ListenAndServeTLS(prx.Settings.Proxy.Out_Adrress, prx.Settings.Proxy.Ssl_Crt, prx.Settings.Proxy.Ssl_Key, mux)
		if err != nil {
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}
		return
	}

	// start the server
	log.Print("Listening HTTP...")
	err = srv.ListenAndServe()
	if err != nil {
		prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
		return
	}
}

func (prx *Proxy) pingApp() error {
	// ping the web app
	res, err := http.Head("http://" + prx.Settings.Proxy.App_Adrress)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// if the web app responded but with an error
	if res.StatusCode != http.StatusOK {
		return err
	}

	return nil
}

func (prx *Proxy) sendReqApp(req *http.Request) (*http.Response, error) {
	// create a client to send to the application
	client := &http.Client{
		// Makes sure the the client DOESN'T auto redirects (VERY IMPORTANT)
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	newReq, err := http.NewRequest(req.Method, "http://"+prx.Settings.Proxy.App_Adrress+req.URL.String(), req.Body)
	if err != nil {
		return nil, err
	}

	newReq.Header = req.Header

	// send to the app
	res, err := client.Do(newReq)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (prx *Proxy) sendResClient(res *http.Response, writer http.ResponseWriter) error {
	// Set the status code and headers of the new response
	for key, values := range res.Header {
		for _, value := range values {
			writer.Header().Add(key, value)
		}
	}

	writer.WriteHeader(res.StatusCode)

	_, err := io.Copy(writer, res.Body)
	return err
}
