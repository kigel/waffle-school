package proxy

import (
	"net/http"
	"strings"
)

func (prx *Proxy) CheckIRA(req *http.Request) bool {
	path := req.URL.String()

	// checking for home page
	if path == "/" {
		return !checkStrInStrArr("/", prx.Settings.Proxy.Resource_Whitelist)
	}

	// did it find IRA
	isIRA := true
	// does the path end with /
	isDir := false

	// checking if dir
	pathParts := strings.Split(path, "/")[1:]
	if pathParts[len(pathParts)-1] == "" {
		isDir = true
		pathParts = pathParts[:len(pathParts)-1]
	}

	originalLen := len(pathParts)

	for range pathParts {
		// joining all the parts
		joinedParts := "/" + strings.Join(pathParts, "/") + "/"
		if len(pathParts) == originalLen && !isDir {
			joinedParts = joinedParts[:len(joinedParts)-1]
		}

		// checking if the joined parts are in the whitelist
		if checkStrInStrArr(joinedParts, prx.Settings.Proxy.Resource_Whitelist) {
			isIRA = false
		}

		// remove last part to check lower hierarchy
		pathParts = pathParts[:len(pathParts)-1]
	}

	return isIRA
}

func checkStrInStrArr(str string, strArr []string) bool {
	for _, checkStr := range strArr {
		if str == checkStr {
			return true
		}
	}
	return false
}
