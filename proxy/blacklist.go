package proxy

import (
	"net/http"

	"gitlab.com/kigel/waffle/mongomanager"
	"go.mongodb.org/mongo-driver/bson"
)

func (prx *Proxy) checkBlacklisted(req *http.Request) (bool, error) {
	ip := mongomanager.ParseAddrToIP(req.RemoteAddr)

	data, err := prx.MongoManager.Get("data", "blacklist", bson.M{"ip": ip})
	if err != nil {
		return false, err
	}

	if len(data) > 0 {
		return true, nil
	}

	return false, nil
}
