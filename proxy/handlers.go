package proxy

import (
	"io"
	"net/http"
	"strings"

	"fmt"

	"gitlab.com/kigel/waffle/attackscanner"
	"gitlab.com/kigel/waffle/errorer"
	"gitlab.com/kigel/waffle/ratelimiter"
)

func (prx *Proxy) rootHandler() http.HandlerFunc {
	return func(writer http.ResponseWriter, req *http.Request) {

		// check if blacklisted
		isBlacklisted, err := prx.checkBlacklisted(req)
		if err != nil {
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}

		// if blacklisted just send them an error page
		if isBlacklisted {
			fmt.Println("Blacklisted Client Was Blocked")
			http.Error(writer, "Forbidden", http.StatusForbidden)
			return
		}

		// check for session cookie

		//getting the session cookie
		cookies := req.Cookies()
		isFound := false
		for _, cookie := range cookies {
			if cookie.Name == prx.Settings.Cookies.SessionCookieName { // add to settings
				isFound = true
				break
			}
		}

		//if didnt found cookie create and return one
		if !isFound {
			//generating a cookie
			cookie, err := CreateSessionCookie(prx.MongoManager)
			if err != nil {
				prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
				return
			}

			//creating an empty http response
			res := &http.Response{
				StatusCode: http.StatusOK,
				Header:     make(http.Header),
				Proto:      "HTTP/1.1",
				ProtoMajor: 1,
				ProtoMinor: 1,
				Request:    req,
				Body:       io.NopCloser(strings.NewReader("")),
			}

			res.Header.Set("Set-Cookie", cookie.String()) // Add new session cookie
			res.Header.Set("Content-Type", "text/plain")
			res.Header.Set("Location", req.URL.Path) // Redirect to same url

			res.StatusCode = http.StatusFound

			prx.MongoManager.InsertResponse(res)

			// Set the status code and headers of the new response
			for key, values := range res.Header {
				for _, value := range values {
					writer.Header().Add(key, value)
				}
			}
			writer.WriteHeader(res.StatusCode)

			// Copy the response body
			_, err = io.Copy(writer, res.Body)
			if err != nil {
				prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
				return
			}

			return
		}

		// rate limiter
		err = ratelimiter.UseTokens(req, prx.MongoManager)
		if err != nil {
			fmt.Println("User Was Out Of Tokens")
			http.Error(writer, "Forbidden", http.StatusForbidden)
			return
		}

		// check if IRA
		isIRA := prx.CheckIRA(req)
		if isIRA {
			fmt.Println("IRA Attack Was Blocked")
			http.Error(writer, "Forbidden", http.StatusForbidden)
			return
		}

		// check if xss
		isXSS, err := attackscanner.ScanXSS(req)
		if err != nil {

			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}

		if isXSS {
			fmt.Println("XSS Attack Was Blocked")
			http.Error(writer, "Forbidden", http.StatusForbidden)
			return
		}

		// check if sqli
		isSQLI, err := attackscanner.ScanSQLI(req)
		if err != nil {
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}

		if isSQLI {
			fmt.Println("SQLI Attack Was Blocked")
			http.Error(writer, "Forbidden", http.StatusForbidden)
			return
		}

		// Send request to app and receive a response
		appRes, err := prx.sendReqApp(req)
		if err != nil {
			fmt.Println("Error Getting A Response From The Server")
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}

		//log normal request
		err = prx.MongoManager.InsertRequest(req, false) // Insert with 'true' if attack found!!!
		if err != nil {
			fmt.Println("Error Logging The Request")
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}

		// Send the response to the client
		err = prx.sendResClient(appRes, writer)
		if err != nil {
			fmt.Println("Error Sending The Response To The Client")
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}
	}
}
