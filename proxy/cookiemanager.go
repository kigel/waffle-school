package proxy

import (
	"net/http"
	"time"

	"github.com/google/uuid"
	"gitlab.com/kigel/waffle/mongomanager"
	"go.mongodb.org/mongo-driver/bson"
)

func CreateSessionCookie(mngman *mongomanager.MongoManager) (*http.Cookie, error) {
	// Create a new cookie
	cookie := http.Cookie{
		Name:    mngman.Settings.Cookies.SessionCookieName,
		Value:   uuid.New().String(),                                                              // random value
		Expires: time.Now().Add(time.Duration(mngman.Settings.Database.Bucket_TTL) * time.Second), // expiery date
		Path:    "/",
	}

	// save the cookie so the server can check if it's valid
	err := mngman.Insert("data", "session_buckets", bson.M{"session_id": cookie.Value, "createdAt": time.Now(), "bucket_size": mngman.Settings.Cookies.SessionCookieSize, "refill_rate": mngman.Settings.Cookies.SessionCookieRate, "current_tokens": mngman.Settings.Cookies.SessionCookieSize, "last_refill": time.Now()})
	if err != nil {
		return nil, err
	}

	return &cookie, nil
}
