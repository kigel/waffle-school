package ratelimiter

import (
	"errors"
	"math"
	"net/http"
	"time"

	"gitlab.com/kigel/waffle/mongomanager"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func UseTokens(req *http.Request, mngman *mongomanager.MongoManager) error {

	//getting the session cookie
	cookies := req.Cookies()
	isFound := false
	sessionCookie := &http.Cookie{}
	for _, cookie := range cookies {
		if cookie.Name == mngman.Settings.Cookies.SessionCookieName { // add to settings
			isFound = true
			sessionCookie = cookie
			break
		}
	}

	//if didnt found cookie return error
	if !isFound {
		return errors.New("No session cookie found! please create a new one...")
	}

	//checking if the cookie exists in the db
	//change to mngman
	_, err := mngman.Get("data", "session_buckets", bson.M{"session_id": sessionCookie.Value})
	if err != nil {
		// return error if no cookie was found --> means that new session cookie needs to be created and this request to be dropped
		return err
	}

	// use tokens from each bucket
	err = removeTokensFromBucket(req, sessionCookie.Value, mngman)
	if err != nil {
		return err
	}

	return nil
}

func removeTokensFromBucket(req *http.Request, filterValue string, mngman *mongomanager.MongoManager) error {
	var collName string
	var key string
	collName = "session_buckets"
	key = "session_id"

	// setting the bucket info
	var maxSize int
	var rate int

	//change to mngman
	bucketInfo, err := mngman.Get("data", collName, bson.M{key: filterValue})
	if err != nil {
		return err
	}

	currSize := int(bucketInfo[0]["current_tokens"].(int32))
	lastFilled := bucketInfo[0]["last_refill"].(primitive.DateTime).Time()

	maxSize = mngman.Settings.Cookies.SessionCookieSize
	rate = mngman.Settings.Cookies.SessionCookieRate

	//calculating the new fields
	currTime := time.Now()
	timeDiff := int((currTime.Sub(lastFilled)).Seconds())

	newSize := currSize + timeDiff*rate
	newCurrSize := int(math.Min(float64(maxSize), float64(newSize)))

	//if cant remove ( not enough tokens) return an error
	tokensToRemove := int(req.ContentLength)
	if (newCurrSize - tokensToRemove) < 0 {
		return errors.New("token bucket depleted")
	}
	newCurrSize = newCurrSize - tokensToRemove

	filter := bson.M{key: filterValue}
	newFields := bson.M{"$set": bson.M{"current_tokens": newCurrSize, "last_refill": currTime}}

	//updating the token bucket
	err = mngman.Update("data", collName, filter, newFields)
	if err != nil {
		return err
	}

	return nil
}
