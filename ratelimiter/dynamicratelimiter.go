package ratelimiter

import (
	"math"
	"time"

	"gitlab.com/kigel/waffle/mongomanager"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	HOUR_TO_SECONDS       = 3600
	RATE_BUFFER           = 2
	RATE_TO_MAX_SIZE      = 100
	TOTAL_SIZE_ARRAY_SIZE = 24
	TIME_ARRAY_SIZE       = 25
)

func GetSessionDynamicRateAndMaxSize(mngman *mongomanager.MongoManager) (int, int, error) {
	//using the roots function because we need the same rate
	rate, _, err := getDynamicRateAndMaxSize(mngman)
	if err != nil {
		return -1, -1, err
	}

	numOfSessions, err := GetTotalNumberOfSessions(mngman)

	//averaging the rate
	sessionRate := rate / numOfSessions

	return sessionRate, sessionRate * RATE_TO_MAX_SIZE, nil // ?maybe change it so we'll get the rate to maxSize convertion rate from the config?
}

func getDynamicRateAndMaxSize(mngman *mongomanager.MongoManager) (int, int, error) {
	var totalSize int
	var err error

	//calling the right getTotalSize function depending on what code we want
	totalSize, err = getTotalSizeOfBussiestHour(mngman)
	if err != nil {
		return -1, -1, err
	}

	//convert totalSize (per hour) to seconds
	sizePerSec := totalSize / HOUR_TO_SECONDS

	//adding a 2x buffer
	rate := sizePerSec * RATE_BUFFER // ?maybe change it so we'll get the buffer multiplier from the config?

	//getting the min possible rate from the config
	var minRate int
	minRate = mngman.Settings.Cookies.SessionCookieRate

	rate = int(math.Max(float64(rate), float64(minRate)))

	//returning the rate, and the max size (rate * 100)
	return rate, rate * RATE_TO_MAX_SIZE, nil // ?maybe change it so we'll get the rate to maxSize convertion rate from the config?
}

func getTotalSizeOfBussiestHour(mngman *mongomanager.MongoManager) (int, error) {
	sizePerHourArr := make([]int, TOTAL_SIZE_ARRAY_SIZE)

	//get the time array (every cell gots the time.Time of every hour in the last 24h period)
	timeArray := generateTimeArray()

	//loop 24 times, once for each hour
	for i := 0; i < TOTAL_SIZE_ARRAY_SIZE; i++ {

		// defining the filter for the date range (between those two time object with an hour between them)
		var filter primitive.M

		filter = getRootFilter(timeArray[i+1], timeArray[i])

		//actually getting the requests between them
		data, err := mngman.Get("logs", "requests", filter)
		if err != nil {
			//if theres an error (there are no fields returned) skip this loops current run
			continue
		}

		// Loop over each request in the array
		for _, req := range data {
			//getting the size of the request
			requestSize := (req["size"]).(int32)
			//adding this current request's size to it's hour in the array's context
			sizePerHourArr[i] += int(requestSize)
		}
	}

	//return the highest number is the array
	maxNum := sizePerHourArr[0]
	for i := 1; i < TOTAL_SIZE_ARRAY_SIZE; i++ {
		if maxNum < sizePerHourArr[i] {
			maxNum = sizePerHourArr[i]
		}
	}

	return maxNum, nil
}

func getRootFilter(startTime, endTime time.Time) primitive.M {
	filter := bson.M{
		"createdAt": bson.M{
			"$gte": startTime, //start date
			"$lte": endTime,   //end date
		},
	}
	return filter
}

// returns an array with the times in the last 24 period in a 1h jump between them (0:00, 1:00, 2:00, ........, 23:00, 0:00 <----- (the direction of the array - backwards))
func generateTimeArray() []time.Time {
	currTime := time.Now()
	timeArray := make([]time.Time, TIME_ARRAY_SIZE)
	timeArray[0] = currTime

	for i := 1; i < TIME_ARRAY_SIZE; i++ {
		// Subtract i hours from the original time
		timeArray[i] = currTime.Add(-time.Duration(i) * time.Hour)
	}

	return timeArray
}

func GetTotalNumberOfSessions(mngman *mongomanager.MongoManager) (int, error) {
	//actually getting the requests between them
	data, err := mngman.Get("waffle_buckets", "session_buckets", bson.M{})
	if err != nil {
		//if theres an error (there are no fields returned) return 0
		return 0, err
	}

	//returning the number of sessions in the map
	return len(data), nil
}
