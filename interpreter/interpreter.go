package interpreter

import (
	"fmt"
	"strings"

	"gitlab.com/kigel/waffle/errorer"
	"gitlab.com/kigel/waffle/mongomanager"
	"gitlab.com/kigel/waffle/settings"
)

type Interpreter struct {
	Settings     *settings.Settings
	MongoManager *mongomanager.MongoManager
	ErrCh        chan errorer.Errorer
}

// Start the Interpreter
func (inter *Interpreter) Start() {
  // stay in loop until user prompts to
	for {
		var input string
    // Scan for user input
		fmt.Scanln(&input)

    // Get the parts
		cmdParts := strings.Split(input, " ")

		switch cmdParts[0] {
		case "exit":
			inter.ErrCh <- errorer.Errorer{Code: errorer.StopAction, Msg: "", Err: nil}
			return
		case "reload":
			inter.ErrCh <- errorer.Errorer{Code: errorer.ReloadAction, Msg: "", Err: nil}
			return
		default:
			fmt.Println("Please enter a valid command!")
		}
	}
}
