// Using the same sqli scanner for now - might change

package attackscanner

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
)

// constants:
var MIN_SYMBOL_DISTANCE int = 5

var MAX_SYMBOL_DISTANCE int = 10

// checks if a http.Request object contains a sqli attack attempt in it's url/body
func ScanSQLI(req *http.Request) (bool, error) {
	//getting the full url
	url1, _ := url.PathUnescape(req.URL.RequestURI())
	url, _ := url.QueryUnescape(url1)
	fullURL := fmt.Sprintf("%s://%s%s", req.URL.Scheme, req.Host, url)
	//if found a payload in the url return true
	if detectSQLIPayload(fullURL) {
		return true, nil
	}

	copiedBody, err := copyReqBody(req)
	if err != nil {
		return false, err
	}

	//gets the request's body
	body, err := io.ReadAll(copiedBody)
	if err != nil {
		return false, err
	}

	//convert the body to a string
	bodyString := string(body)

	//returns the result of the detect sql payload on the body
	return detectSQLIPayload(bodyString), nil
}

/*
****** Will be better to create a db table for sqli signatures so it will be dynamic instead of manually testing each string (will be used for every test not just the sqli keywords) ******
 */

// given a string, checks if it contains a sqli attack attempt
func detectSQLIPayload(str_to_check string) bool {
	//converting the str to lower case
	lower_case_str := strings.ToLower(str_to_check)

	//sqli signatures
	sqli_keywords := []string{"'", "or", "select", "=", "union", "sort", "order", "by", "from", "where", "*", "--", "if", "concat", "sleep", "benchmark", "true", "false", "grant", ";", "drop", "insert", "_"}

	suspicious_str := false

	//checking if the keyword is in the given str
	for _, keyword := range sqli_keywords {
		//if in recognized as suspisous
		if strings.Contains(lower_case_str, keyword) {
			suspicious_str = true
			break
		}
	}

	//if there are no sqli keywords in the str at all, return false - not flagged as sqli
	if !suspicious_str {
		return false
	}

	//bigger filters: checking for critical patterns:

	//critical pattern #1 'select * from'
	if strings.Contains(lower_case_str, "select * from") {
		return true
	}
	//critical pattern #2 'or 1=1'
	if strings.Contains(lower_case_str, "or 1=1") {
		return true
	}
	//critical pattern #3 '@@version'
	if strings.Contains(lower_case_str, "@@version") {
		return true
	}
	//critical pattern #4 'waitfor delay'
	if strings.Contains(lower_case_str, "waitfor delay") {
		return true
	}
	//critical pattern #5 'xp_cmdshell'
	if strings.Contains(lower_case_str, "xp_cmdshell") {
		return true
	}
	//critical pattern #6 'insert into'
	if strings.Contains(lower_case_str, "insert into") {
		return true
	}
	//critical pattern #7 'sp_configure'
	if strings.Contains(lower_case_str, "sp_configure") {
		return true
	}
	//critical pattern #8 'drop procedure'
	if strings.Contains(lower_case_str, "drop procedure") {
		return true
	}
	//critical pattern #9 'echo'
	if strings.Contains(lower_case_str, "echo") {
		return true
	}
	//critical pattern #10 'drop login'
	if strings.Contains(lower_case_str, "drop login") {
		return true
	}
	//critical pattern #11 'sp_addlogin'
	if strings.Contains(lower_case_str, "sp_addlogin") {
		return true
	}
	//critical pattern #12 'drop index'
	if strings.Contains(lower_case_str, "drop index") {
		return true
	}
	//critical pattern #13 'sp_rename'
	if strings.Contains(lower_case_str, "sp_rename") {
		return true
	}

	//smaller filters: checking for unusual patterns:

	//filter #1: or number = number
	if check_distance(lower_case_str, " or", "=", MIN_SYMBOL_DISTANCE) {
		return true
	}
	//filter #2 SELECT IF
	if check_distance(lower_case_str, "select", "if", MAX_SYMBOL_DISTANCE) {
		return true
	}
	//filter #3 if number = number
	if check_distance(lower_case_str, "if", "=", MIN_SYMBOL_DISTANCE) {
		return true
	}
	//filer #4 select concat
	if check_distance(lower_case_str, "select", "concat", MAX_SYMBOL_DISTANCE) {
		return true
	}
	//filter #5 union select
	if check_distance(lower_case_str, "union", "select", MAX_SYMBOL_DISTANCE) {
		return true
	}
	//filter #6 ' or
	if check_distance(lower_case_str, "'", "or", MIN_SYMBOL_DISTANCE) {
		return true
	}
	//filter #7 ' --
	if check_distance(lower_case_str, "'", "--", MIN_SYMBOL_DISTANCE) {
		return true
	}
	//filter #8 ' #
	if check_distance(lower_case_str, "'", "#", MIN_SYMBOL_DISTANCE) {
		return true
	}
	//filter #9 or true
	if check_distance(lower_case_str, "or", "true", MIN_SYMBOL_DISTANCE) {
		return true
	}
	//filter #10 drop table
	if check_distance(lower_case_str, "drop", "table", MIN_SYMBOL_DISTANCE) {
		return true
	}
	//filter #11 where =
	if check_distance(lower_case_str, "where", "=", MAX_SYMBOL_DISTANCE) {
		return true
	}
	//filter #12 set =
	if check_distance(lower_case_str, "set", "=", MAX_SYMBOL_DISTANCE) {
		return true
	}
	//filter #13 ; --
	if check_distance(lower_case_str, ";", "--", MIN_SYMBOL_DISTANCE) {
		return true
	}

	//if passed all -> return false
	return false
}

// checks if the distance between 2 words/phrases in a string is within a certain range (goes over all instances of those two phrases)
func check_distance(str, word1, word2 string, minDistance int) bool {
	// Initialize indices for the first occurrence of word1 and word2
	index1 := strings.Index(str, word1)
	index2 := strings.Index(str, word2)

	// Loop as long as both word1 and word2 are found in the string
	for index1 != -1 {
		// Iterate through all occurrences of word2
		for index2 != -1 {
			// Calculate the distance between the end of word1 and the start of word2
			distance := absInt(index2 - (index1 + len(word1)))

			// Check if the distance satisfies the minimum distance condition given to the function
			if distance < minDistance {
				return true
			}

			// Update index2 to the next occurrence of word2
			nextIndex2 := strings.Index(str[index2+len(word2):], word2)
			if nextIndex2 != -1 {
				index2 += len(word2) + nextIndex2
			} else {
				index2 = -1
			}
		}

		// Update index1 to the next occurrence of word1
		nextIndex1 := strings.Index(str[index1+len(word1):], word1)
		if nextIndex1 != -1 {
			index1 += len(word1) + nextIndex1
		} else {
			index1 = -1
		}

		// Reset index2 for the next iteration of word1
		index2 = strings.Index(str, word2)
	}

	// If no satisfying distance is found, return false
	return false
}

// returns the absolute value of a given number
func absInt(num int) int {
	if num < 0 {
		return -num
	}
	return num
}
