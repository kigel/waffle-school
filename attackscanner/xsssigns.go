package attackscanner

// ALL OF THE SIGNATURES TAKEN FROM libinjection C LIBRARY

var (
	// Banned tag names
	BANNED_TAG = []string{
		"APPLET",
		"BASE",
		"COMMENT",
		"EMBED",
		"FRAME",
		"FRAMESET",
		"HANDLER",
		"IFRAME",
		"IMPORT",
		"ISINDEX",
		"LINK",
		"LISTENER",
		"META",
		"NOSCRIPT",
		"OBJECT",
		"SCRIPT",
		"STYLE",
		"VMLFRAME",
		"XML",
		"XSS",
		"FILTER",
		"STYLE",
		"SVG",
		"XSL",
	}

	// Banned attrs keys
	BANNED_ATTR_KEY = []string{
		"DATAFORMATAS",
		"DATASRC",
	}

	// Attrs keys that have a url(like) value
	ATTR_KEY_URL = []string{
		"ACTION",
		"BY",
		"BACKGROUND",
		"DYNSRC",
		"FORMACTION",
		"FOLDER",
		"FROM",
		"HANDLER",
		"HREF",
		"LOWSRC",
		"POSTER",
		"SRC",
		"TO",
		"VALUES",
	}

	// Banned url prefix values (javascript:http://foo.com/bar.js)
	BANNED_URL_PREFIX = []string{
		"DATA",
		"VIEW-SOURCE",
		"VBSCRIPT",
		"JAVA",
		"JAVASCRIPT",
	}
)
