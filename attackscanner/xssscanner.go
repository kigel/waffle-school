package attackscanner

import (
	"bytes"
	"io"
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/net/html"
)

func ScanXSS(req *http.Request) (bool, error) {
	// copy the body so it wont break
	copiedBody, err := copyReqBody(req)
	if err != nil {
		return false, err
	}

	// check body
	if tokenizeAndCheckXSS(copiedBody) {
		return true, nil
	}

	// decode the url
	decodedUrl, err := url.PathUnescape(req.URL.String()) // didnt do anything with the err
	decodedUrl, err = url.QueryUnescape(decodedUrl)
	if err != nil {
		return false, nil
	}

	// check url
	urlReader := bytes.NewReader([]byte(decodedUrl))
	if tokenizeAndCheckXSS(urlReader) {
		return true, nil
	}

	return false, nil
}

func tokenizeAndCheckXSS(data io.Reader) bool {
	z := html.NewTokenizer(data)
	for {
		tt := z.Next()
		// eof
		if tt == html.ErrorToken {
			if z.Err() == io.EOF {
				// didn't find any xss injections
				return false
			}
		}
		token := z.Token()

		if token.Type != html.StartTagToken && token.Type != html.EndTagToken {
			continue
		}

		/*
			Check Tag Name
		*/

		if checkIfBannedTagName(token.Data) {
			return true
		}

		/*
			Check Attrs
		*/
		if len(token.Attr) == 0 {
			continue
		}

		for _, attr := range token.Attr {
			if checkIfBannedAttrKey(attr.Key) {
				return true
			}

			if attr.Val == "" {
				continue
			}

			if checkIfUrlAttrKey(attr.Key) {
				if checkIfBannedUrl(attr.Val) {
					return true
				}
			}
		}
	}

}

func checkIfBannedTagName(tagName string) bool {
	tagName = formatStr(tagName)

	// there is no banned tag name that is less than 3 chars
	if len(tagName) < 3 {
		return false
	}

	return checkStrInStrArr(tagName, BANNED_TAG)
}

func checkIfBannedAttrKey(attrKey string) bool {
	attrKey = formatStr(attrKey)

	// there is no banned attr key that is less than 2 chars
	if len(attrKey) < 2 {
		return false
	}

	// on* and XMLNS and XLINK will always be more/equal 5
	if len(attrKey) >= 5 {
		// check if attr is on* (onclick, onerror...)
		if attrKey[:2] == "ON" {
			return true
		}

		// check if attr starts with XMLNS or XLINK
		if attrKey[:5] == "XMLNS" || attrKey[:5] == "XLINK" {
			return true
		}

	}

	return checkStrInStrArr(attrKey, BANNED_ATTR_KEY)
}

func checkIfUrlAttrKey(attrKey string) bool {
	attrKey = formatStr(attrKey)

	return checkStrInStrArr(attrKey, ATTR_KEY_URL)
}

func checkIfBannedUrl(attrVal string) bool {
	attrVal = formatStr(attrVal)

	// check suffix for js (bar.js)
	suffixArr := strings.Split(attrVal, ".")
	if len(suffixArr) > 1 {
		suffix := suffixArr[len(suffixArr)]
		if suffix == "JS" {
			return true
		}
	}

	// check for prefix (javascript:URL)
	prefix := strings.Split(attrVal, ":")[0]
	return checkStrInStrArr(prefix, BANNED_URL_PREFIX)
}

func copyReqBody(req *http.Request) (io.Reader, error) {
	bodyContent, err := io.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}

	// restore the body in the request
	req.Body = io.NopCloser(bytes.NewBuffer(bodyContent))

	// Remove URL Encoding for testing (will be reencoded in the encoder package)
	decoded, _ := url.QueryUnescape(string(bodyContent))
	bodyContent = []byte(decoded)

	// return a copy of the body
	return bytes.NewReader(bodyContent), nil
}

func checkStrInStrArr(str string, strArr []string) bool {
	for _, checkStr := range strArr {
		if str == checkStr {
			return true
		}
	}
	return false
}

func formatStr(str string) string {
	newStr := strings.ToUpper(str)
	return newStr
}
