package settings

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Settings struct {
	Proxy struct {
		App_Adrress        string   `yaml:"app_address"`
		Out_Adrress        string   `yaml:"out_address"`
		Ssl_Crt            string   `yaml:"ssl_crt"`
		Ssl_Key            string   `yaml:"ssl_key"`
		Resource_Whitelist []string `yaml:"resource_whitelist"`
	}
	Database struct {
		Db_Adrress string `yaml:"db_adrress"`
		Bucket_TTL int32  `yaml:"bucket_ttl"`
		Log_TTL    int32  `yaml:"log_ttl"`
	}
	Cookies struct {
		SessionCookieName string `yaml:"cookie_name"`
		SessionCookieRate int    `yaml:"cookie_rate"`
		SessionCookieSize int    `yaml:"cookie_size"`
	}
	DdosScanner struct {
		MinCount               int `yaml:"min_count"`
		MulValue               int `yaml:"mul_value"`
		MinReqsPerClient       int `yaml:"min_reqs_per_client"`
		MinPercentagePerClient int `yaml:"min_percenrage_per_client"`
		TimePeriod             int `yaml:"time_period"`
	}
}

// Parses settings into a struct from the settings path provided
func GetSettings(settingsPath string) (*Settings, error) {
	settingsRaw, err := os.ReadFile(settingsPath)
	if err != nil {
		return nil, err
	}

	var sett Settings
	if err = yaml.Unmarshal(settingsRaw, &sett); err != nil {
		return nil, err
	}

	return &sett, nil
}
