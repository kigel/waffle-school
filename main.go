package main

import (
	"log"

	// "gitlab.com/kigel/waffle/ddosscanner"
	"gitlab.com/kigel/waffle/ddosscanner"
	"gitlab.com/kigel/waffle/errorer"
	"gitlab.com/kigel/waffle/interpreter"
	"gitlab.com/kigel/waffle/mongomanager"
	"gitlab.com/kigel/waffle/proxy"
	"gitlab.com/kigel/waffle/settings"
)

func main() {
	stop := false

	for !stop {
		// get the settings from the file
		log.Print("Parsing settings from file...")
		sett, err := settings.GetSettings("./settings.yaml")
		if err != nil {
			log.Fatal(errorer.Errorer{Code: errorer.SetupError, Msg: "No settings file was found", Err: err})
		}
		log.Print("Settings parsed!")

		// setup the dbmanager
		log.Print("Connecting db...")
		mngman := &mongomanager.MongoManager{Settings: sett}
		err = mngman.Connect()
		if err != nil {
			log.Fatal(errorer.Errorer{Code: errorer.SetupError, Msg: "Mongo didn't connect", Err: err})
		}
		log.Print("Db connection valid!")

		// make error channel and settings mutex
		errCh := make(chan errorer.Errorer, 10)
		wafProxy := proxy.Proxy{Settings: sett, MongoManager: mngman, ErrCh: errCh}
		go wafProxy.Start()
		inter := interpreter.Interpreter{Settings: sett, MongoManager: mngman, ErrCh: errCh}
		go inter.Start()

		wafDdosScanner := ddosscanner.DdosScanner{MongoManager: mngman}
		go wafDdosScanner.ScanDdos()

		// wait for errors
		errer := <-errCh

		switch errer.Code {
		case errorer.StopAction:
			stop = true
		case errorer.ReloadAction:
			stop = false
		default:
			log.Print(errer)
			stop = true
		}

		closeWaf(&wafProxy)
		close(errCh)
	}
}

func closeWaf(prx *proxy.Proxy) {
	log.Println("Closing WAF...")
	// disconnect from the db
	log.Print("Disconnecting from db...")
	err := prx.MongoManager.Disconnect()
	if err != nil {
		log.Fatal(err)
	}
	log.Print("Db disconnection successful!")
	// close the waf
	log.Print("Closing server...")
	err = prx.Server.Close()
	if err != nil {
		log.Print(&errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err})
	}
	log.Print("Server closed successfully!")
}
